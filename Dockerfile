FROM openjdk:11.0-jre
COPY . /app/
WORKDIR /app

ENTRYPOINT ["java", "-jar", "devops-0.0.1-SNAPSHOT.jar"]

